alter table my_location add has_Altitude boolean;
alter table my_location add has_Speed Boolean;
alter table my_location add has_Bearing Boolean;
alter table my_location add has_Accuracy Boolean;