package learning.nourbaha.sematecadvance;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.IBinder;
import android.support.annotation.Nullable;

import java.util.Calendar;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.location.config.LocationParams;
import learning.nourbaha.sematecadvance.dbmodels.MyLocation;


/**
 * Created by Nourbaha on 2/11/2018.
 */

public class LocationService extends Service {
    Context mContext = this;
    long count;

    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        getLocatioan(intent);
        //stopSelf();
        return super.onStartCommand(intent, flags, startId);

    }

    private void getLocatioan(Intent intent) {
        count = 0;
        SmartLocation.with(this).location().config(LocationParams.BEST_EFFORT).
                start(new OnLocationUpdatedListener() {
                    @Override
                    public void onLocationUpdated(Location location) {
                        MyLocation myLocation = new MyLocation();

                        myLocation.setLocalDateTime(Calendar.getInstance().getTime());
                        myLocation.setLatitude(location.getLatitude());
                        myLocation.setLongitude(location.getLongitude());
                        myLocation.setAltitude(location.getAltitude());
                        myLocation.setHasAltitude(location.hasAltitude());
                        myLocation.setAccuracy(location.getAccuracy());
                        myLocation.setHasAccuracy(location.hasAccuracy());
                        myLocation.setBearing(location.getBearing());
                        myLocation.setHasBearing(location.hasBearing());
                        myLocation.setSpeed(location.getSpeed());
                        myLocation.setHasSpeed(location.hasSpeed());
                        myLocation.setTime(location.getTime());

                        myLocation.save();

                        count += 1;
                        PublicMethods.showToast(mContext, "Data Store count : " + count);
                    }
                });

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
