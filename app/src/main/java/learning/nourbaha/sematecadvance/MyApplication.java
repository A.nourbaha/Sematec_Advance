package learning.nourbaha.sematecadvance;

import android.Manifest;
import android.app.Application;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.orhanobut.hawk.Hawk;
import com.orm.SugarApp;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Nourbaha on 2/9/2018.
 */

public class MyApplication extends SugarApp {
    public static Typeface typeface ;

    @Override
    public void onCreate() {
        super.onCreate();
        typeface = Typeface.createFromAsset(getAssets(),Constants.font);
        Hawk.init(this).build();
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name("sematecAdvance.realm")
//                .encryptionKey(getKey())
//                .schemaVersion(42)
//                .modules(new MySchemaModule())
//                .migration(new MyMigration())
                .build();
        Realm.setDefaultConfiguration(config);
    }
}
