package learning.nourbaha.sematecadvance.adapters;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;
import android.content.Context;
import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;

import java.util.List;

import learning.nourbaha.sematecadvance.utils.library.SvgSoftwareLayerSetter;
import learning.nourbaha.sematecadvance.R;
import learning.nourbaha.sematecadvance.webmodels.Standing;

/**
 * Created by Nourbaha on 2/4/2018.
 */

public class LeagueTableAdapter extends RecyclerView.Adapter<LeagueTableAdapter.MyHolder> {
    private Context context;
    private List<Standing> standings;

    public LeagueTableAdapter(Context context, List<Standing> standings) {
        this.context = context;
        this.standings = standings;
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).
                inflate(R.layout.team_item, parent, false);

        return new MyHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyHolder holder, int position) {
        holder.teamName.setText(standings.get(position).getTeamName());
        holder.points.setText("Points : "+standings.get(position).getPoints().toString());
        holder.goalDifference.setText("Goal Difference : "+standings.get(position).getGoalDifference().toString());
        holder.position.setText(standings.get(position).getPosition().toString());

//        image

        RequestBuilder<PictureDrawable> requestBuilder;
        requestBuilder = Glide.with(context)
                .as(PictureDrawable.class)
//                .placeholder(R.drawable.image_loading)
//                .error(R.drawable.image_error)
                .transition(withCrossFade())
                .listener(new SvgSoftwareLayerSetter());

        Uri uri = Uri.parse(standings.get(position).getCrestURI());
        requestBuilder
//                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .load(uri)
                .into(holder.image);





    }

    @Override
    public int getItemCount() {
        return standings.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView teamName;
        TextView points;
        TextView goalDifference;
        TextView position;

        public MyHolder(View itemView) {
            super(itemView);

             image = itemView.findViewById(R.id.image);
             teamName = itemView.findViewById(R.id.teamName);
             points = itemView.findViewById(R.id.points);
             goalDifference = itemView.findViewById(R.id.goalDifference);
             position = itemView.findViewById(R.id.position);
        }
    }
}
