package learning.nourbaha.sematecadvance.leaguetable;

import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.orhanobut.hawk.Hawk;

import learning.nourbaha.sematecadvance.BaseActivity;
import learning.nourbaha.sematecadvance.PublicMethods;
import learning.nourbaha.sematecadvance.R;
import learning.nourbaha.sematecadvance.adapters.LeagueTableAdapter;
import learning.nourbaha.sematecadvance.splashscreen.SplashScreenActivity;
import learning.nourbaha.sematecadvance.utils.views.MyButton;
import learning.nourbaha.sematecadvance.webmodels.LeagueTable;

public class LeagueActivity extends BaseActivity implements LeagueContract.View, View.OnClickListener {

    TextView leagueName;
    RecyclerView leagueList;
    MyButton logout;
    Boolean close = false;

    LeaguePresenter presenter = new LeaguePresenter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_league);

        presenter.attachView(this);
        bindView();
        CheckPermision();
        presenter.onLoad();
    }

    private void bindView() {
        leagueName = (TextView) findViewById(R.id.leagueName);
        leagueList = (RecyclerView) findViewById(R.id.leagueList);
        logout = (MyButton) findViewById(R.id.logout);
        logout.setOnClickListener(this);
    }

    private void CheckPermision() {
        //Don't need for internet
    }

//    private void getData() {
//        AsyncHttpClient client = new AsyncHttpClient();
//        client.get(Constants.leagueURL, new TextHttpResponseHandler() {
//            @Override
//            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
//                PublicMethods.showToast(mContext, throwable.getMessage());
//            }
//
//            @Override
//            public void onSuccess(int statusCode, Header[] headers, String responseString) {
//                //PublicMethods.showToast(mContext,responseString);
//                LeagueTable leagueTable =
//                        new Gson().fromJson(responseString, LeagueTable.class);
////                PublicMethods.showToast(mContext,leagueTable.getLeagueCaption());
//                leagueName.setText(leagueTable.getLeagueCaption());
//                LeagueTableAdapter adapter =
//                        new LeagueTableAdapter(mContext, leagueTable.getStanding());
//                leagueList.setAdapter(adapter);
//                RecyclerView.LayoutManager layoutManager =
//                        new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
//                leagueList.setLayoutManager(layoutManager);
//            }
//        });
//    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.logout) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Hawk.delete("token");
                    startActivity(new Intent(mContext, SplashScreenActivity.class));
                    finish();
                }
            }, 1000);
        }
    }

    @Override
    public void onBackPressed() {
        if (close)
            super.onBackPressed();
        else {
            close = true;
            PublicMethods.showToast(mContext, "برای خروج، دوباره کلید بازگشت را فشار دهید");
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                close = false;
            }
        }, 2000);

    }

    @Override
    public void onLoadSuccess(LeagueTable leagueTable) {
        leagueName.setText(leagueTable.getLeagueCaption());
        LeagueTableAdapter adapter =
                new LeagueTableAdapter(mContext, leagueTable.getStanding());
        leagueList.setAdapter(adapter);
        RecyclerView.LayoutManager layoutManager =
                new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        leagueList.setLayoutManager(layoutManager);
    }

    @Override
    public void onLoadFailed(String errorMessage) {
        PublicMethods.showToast(mContext, errorMessage);
    }
}
