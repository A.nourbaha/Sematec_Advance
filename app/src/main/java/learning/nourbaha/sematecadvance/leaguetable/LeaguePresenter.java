package learning.nourbaha.sematecadvance.leaguetable;

import learning.nourbaha.sematecadvance.webmodels.LeagueTable;

/**
 * Created by Nourbaha on 2/25/2018.
 */

public class LeaguePresenter implements LeagueContract.Presenter {
    private LeagueContract.View view;
    LeagueModel model = new LeagueModel();

    @Override
    public void attachView(LeagueContract.View view) {
        this.view = view;
        model.attachPresenter(this);
    }

    @Override
    public void detach() {
        this.view = null;
    }

    @Override
    public void onLoad() {
        view.onProgress(true);
        model.onLoad();
    }

    @Override
    public void onLoadSuccess(LeagueTable leagueTable) {
        view.onLoadSuccess(leagueTable);
        view.onProgress(false);
    }

    @Override
    public void onLoadFailed(String errorMessage) {
        view.onLoadFailed(errorMessage);
        view.onProgress(false);
    }
}
