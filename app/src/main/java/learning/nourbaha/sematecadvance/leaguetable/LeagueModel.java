package learning.nourbaha.sematecadvance.leaguetable;

import io.realm.Realm;
import io.realm.RealmResults;
import learning.nourbaha.sematecadvance.Constants;
import learning.nourbaha.sematecadvance.webmodels.LeagueTable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Nourbaha on 2/25/2018.
 */

public class LeagueModel implements LeagueContract.Model {
    private LeagueContract.Presenter presenter;
    Realm realm = Realm.getDefaultInstance();

    @Override
    public void attachPresenter(LeagueContract.Presenter presenter) {

        this.presenter = presenter;
    }

    @Override
    public void onLoad() {
        Constants.webInterfaceLeagueTable.loadLeagueTable(21).enqueue(new Callback<LeagueTable>() {
            @Override
            public void onResponse(Call<LeagueTable> call, Response<LeagueTable> response) {
                onSave(response.body());
                presenter.onLoadSuccess(response.body());
            }

            @Override
            public void onFailure(Call<LeagueTable> call, Throwable t) {
                //Get data from DB
                onLoadOffline(t);

                //if not ok -- show error message
//                presenter.onLoadFailed(t.getMessage());
            }
        });
    }

//    @Override
    public void onSave(final LeagueTable leagueTable) {
//        realm.beginTransaction();
//        realm.copyToRealm(leagueTable);
//        realm.commitTransaction();

        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm bgRealm) {

//                LeagueTable leagueTable1 = bgRealm.createObject(LeagueTable.class);
//                leagueTable1 =
                        bgRealm.copyToRealm(leagueTable);
//                leagueTable1 = leagueTable;
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                String st = "";
                // Transaction was a success.
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                String st = "";
                // Transaction failed and was automatically canceled.
            }
        });
    }

    private void onLoadOffline(Throwable t){
        LeagueTable result =
                realm.where(LeagueTable.class).equalTo("matchday",21).findFirst();
        if (result != null)
        presenter.onLoadSuccess(result);
        else
            presenter.onLoadFailed(t.getMessage());
    }
}
