package learning.nourbaha.sematecadvance.leaguetable;


import io.realm.internal.Table;
import learning.nourbaha.sematecadvance.webmodels.LeagueTable;

/**
 * Created by Nourbaha on 2/25/2018.
 */

public interface LeagueContract {
    interface View {
        void onLoadSuccess(LeagueTable leagueTable);
        void onLoadFailed(String errorMessage);
        void onProgress(Boolean show);
    }

    interface Presenter {
        void attachView(View view);
        void detach();
        void onLoad();
        void onLoadSuccess(LeagueTable leagueTable);
        void onLoadFailed(String errorMessage);
    }

    interface Model {
        void attachPresenter(Presenter presenter);
        void onLoad();
//        void onSave(LeagueTable leagueTable);
    }
}
