package learning.nourbaha.sematecadvance;

import learning.nourbaha.sematecadvance.utils.RetrofitServiceGenrator;
import learning.nourbaha.sematecadvance.utils.WebInterface;

/**
 * Created by Nourbaha on 2/3/2018.
 */

public class Constants {
    public static final String leagueURL =
            "http://api.football-data.org/v1/competitions/452/leagueTable/?matchday=21";
    public static final String font = "fonts/pano_medium.ttf";
    public static WebInterface webInterface =
            RetrofitServiceGenrator.createService(WebInterface.class);
    public static WebInterface webInterfaceLeagueTable =
            RetrofitServiceGenrator.createServiceLeagueTable(WebInterface.class);
    public static WebInterface webInterfaceWithToken =
            RetrofitServiceGenrator.createServiceWithToken(WebInterface.class);
}
