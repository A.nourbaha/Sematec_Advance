package learning.nourbaha.sematecadvance;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import cz.msebera.android.httpclient.Header;
import learning.nourbaha.sematecadvance.adapters.LeagueTableAdapter;
import learning.nourbaha.sematecadvance.webmodels.LeagueTable;

public class MainActivity extends BaseActivity {

//    Button showLeague;
    TextView leagueName;
    RecyclerView leagueList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bindView();

    }

    private void bindView() {

        leagueName = (TextView) findViewById(R.id.leagueName);
        leagueList = (RecyclerView) findViewById(R.id.leagueList);

        findViewById(R.id.showLeague).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CheckPermision();
                getData();

            }
        });
    }

    private void CheckPermision() {
        //Don't need for internet
    }

    private void getData() {
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(Constants.leagueURL, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                PublicMethods.showToast(mContext,throwable.getMessage());
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                //PublicMethods.showToast(mContext,responseString);
                LeagueTable leagueTable =
                        new Gson().fromJson(responseString,LeagueTable.class);
//                PublicMethods.showToast(mContext,leagueTable.getLeagueCaption());
                leagueName.setText(leagueTable.getLeagueCaption());
                LeagueTableAdapter adapter =
                        new LeagueTableAdapter(mContext,leagueTable.getStanding());
                leagueList.setAdapter(adapter);
                RecyclerView.LayoutManager layoutManager =
                        new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
                leagueList.setLayoutManager(layoutManager);
            }
        });
    }
}
