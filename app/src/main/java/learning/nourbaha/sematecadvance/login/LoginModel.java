package learning.nourbaha.sematecadvance.login;

import com.orhanobut.hawk.Hawk;

import java.sql.Date;

import learning.nourbaha.sematecadvance.Constants;
import learning.nourbaha.sematecadvance.webmodels.LoginPojoModel;
import learning.nourbaha.sematecadvance.webmodels.TokenPojoModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Nourbaha on 2/9/2018.
 */

public class LoginModel implements LoginContract.Model {
    private LoginContract.Presenter presenter;

    @Override
    public void attachPresenter(LoginContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void login(String user, String pass) {
        Constants.webInterface.login("login", user, pass).enqueue(new Callback<TokenPojoModel>() {
            @Override
            public void onResponse(Call<TokenPojoModel> call, Response<TokenPojoModel> response) {
                Hawk.put("token", response.body().getToken());
                presenter.onLoginSuccess();
            }

            @Override
            public void onFailure(Call<TokenPojoModel> call, Throwable t) {
                presenter.onLoginFailed
                        ("User in not fined - Error message : " + t.getMessage());
            }
        });
    }
}
