package learning.nourbaha.sematecadvance.login;

import learning.nourbaha.sematecadvance.webmodels.LoginPojoModel;

/**
 * Created by Nourbaha on 2/9/2018.
 */

public interface LoginContract {

    interface View {
        void login(String user, String pass);
        void onLoginSuccess();
        void onLoginFailed(String errorMessage);
        void onProgress(Boolean show);
    }

    interface Presenter {
        void attachView(View view);
        void detach();

        void login(String user, String pass);
        void onLoginSuccess();
        void onLoginFailed(String errorMessage);

    }

    interface Model {
        void attachPresenter(Presenter presenter);

        void login(String user, String pass);
    }
}
