package learning.nourbaha.sematecadvance.login;

import learning.nourbaha.sematecadvance.webmodels.LoginPojoModel;

/**
 * Created by Nourbaha on 2/10/2018.
 */

public class LoginPresenter implements LoginContract.Presenter {
    private LoginContract.View view ;
    LoginModel model = new LoginModel();

    @Override
    public void attachView(LoginContract.View view) {
        this.view = view;
        this.model.attachPresenter(this);
    }

    @Override
    public void detach() {
        this.view = null;
    }

    @Override
    public void login(String user, String pass) {
        this.view.onProgress(true);
        this.model.login(user,pass);
    }

    @Override
    public void onLoginSuccess() {
        this.view.onLoginSuccess();
        this.view.onProgress(false);
    }

    @Override
    public void onLoginFailed(String errorMessage) {
        this.view.onLoginFailed(errorMessage);
        this.view.onProgress(false);
    }
}
