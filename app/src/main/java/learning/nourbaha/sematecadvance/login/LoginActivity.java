package learning.nourbaha.sematecadvance.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import learning.nourbaha.sematecadvance.BaseActivity;
import learning.nourbaha.sematecadvance.PublicMethods;
import learning.nourbaha.sematecadvance.R;
import learning.nourbaha.sematecadvance.searchLocationForWeather.LocationOnMapActivity;
import learning.nourbaha.sematecadvance.utils.views.MyButton;
import learning.nourbaha.sematecadvance.utils.views.MyEditText;

public class LoginActivity extends BaseActivity implements LoginContract.View, View.OnClickListener {

    LoginPresenter presenter = new LoginPresenter();

    MyEditText user;
    MyEditText pass;
    MyButton login;

    ProgressDialog progressDialog ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        bindView();
    }

    private void bindView() {
        user = (MyEditText) findViewById(R.id.user);
        pass = (MyEditText) findViewById(R.id.pass);
        login = (MyButton) findViewById(R.id.login);

        progressDialog = new ProgressDialog(mContext);

        login.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.attachView(this);

    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.detach();
    }

    @Override
    public void login(String user, String pass) {
        presenter.login(user,pass);
    }

    @Override
    public void onLoginSuccess() {
        PublicMethods.showToast
                (mContext,"Login successfully");
        //Go to next page
        startActivity(new Intent(mContext, LocationOnMapActivity.class));
        finish();
    }

    @Override
    public void onLoginFailed(String errorMessage) {
        PublicMethods.showToast(mContext,errorMessage);
    }

    @Override
    public void onProgress(Boolean show) {
        if (show)
            progressDialog.show();
        else
            progressDialog.cancel();
    }

    @Override
    public void onClick(View view) {
        login(user.text(),pass.text());
    }
}
