package learning.nourbaha.sematecadvance.dbmodels;

import com.orm.SugarRecord;

import java.util.Date;

/**
 * Created by Nourbaha on 2/11/2018.
 */

public class MyLocation extends SugarRecord<MyLocation> {

    private Date localDateTime;
    private double latitude;
    private double longitude;
    private double altitude;
    private float speed;
    private float bearing;
    private float accuracy;
    private Boolean hasAltitude;
    private Boolean hasSpeed;
    private Boolean hasBearing;
    private Boolean hasAccuracy;
    private long time;

    public MyLocation() {
    }

    public MyLocation(Date localDateTime, double latitude, double longitude,
                      double altitude, float speed, float bearing,
                      float accuracy, Boolean hasAltitude, Boolean hasSpeed,
                      Boolean hasBearing, Boolean hasAccuracy, long time) {
        this.localDateTime = localDateTime;
        this.latitude = latitude;
        this.longitude = longitude;
//        this.type = type;
        this.altitude = altitude;
        this.speed = speed;
        this.bearing = bearing;
        this.accuracy = accuracy;
        this.hasAltitude = hasAltitude;
        this.hasSpeed = hasSpeed;
        this.hasBearing = hasBearing;
        this.hasAccuracy = hasAccuracy;
        this.time = time;
    }

    public Date getLocalDateTime() {
        return localDateTime;
    }

    public void setLocalDateTime(Date localDateTime) {
        this.localDateTime = localDateTime;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public float getBearing() {
        return bearing;
    }

    public void setBearing(float bearing) {
        this.bearing = bearing;
    }

    public float getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(float accuracy) {
        this.accuracy = accuracy;
    }

    public Boolean hasAltitude() {
        return hasAltitude;
    }

    public void setHasAltitude(Boolean hasAltitude) {
        this.hasAltitude = hasAltitude;
    }

    public Boolean hasSpeed() {
        return hasSpeed;
    }

    public void setHasSpeed(Boolean hasSpeed) {
        this.hasSpeed = hasSpeed;
    }

    public Boolean hasBearing() {
        return hasBearing;
    }

    public void setHasBearing(Boolean hasBearing) {
        this.hasBearing = hasBearing;
    }

    public Boolean hasAccuracy() {
        return hasAccuracy;
    }

    public void setHasAccuracy(Boolean hasAccuracy) {
        this.hasAccuracy = hasAccuracy;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
