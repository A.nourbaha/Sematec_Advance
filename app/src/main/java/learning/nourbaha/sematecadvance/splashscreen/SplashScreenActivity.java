package learning.nourbaha.sematecadvance.splashscreen;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;

import com.orhanobut.hawk.Hawk;

import io.nlopez.smartlocation.SmartLocation;
import learning.nourbaha.sematecadvance.BaseActivity;
import learning.nourbaha.sematecadvance.LocationService;
import learning.nourbaha.sematecadvance.R;
import learning.nourbaha.sematecadvance.searchLocationForWeather.LocationOnMapActivity;
import learning.nourbaha.sematecadvance.login.LoginActivity;
import learning.nourbaha.sematecadvance.register.RegisterActivity;

public class SplashScreenActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

//        checkPermission();
//        checkDevice();
//        startServices();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//                startActivity(new Intent(mContext, UserReportActivity.class));
                Dispatcher();
            }
        },2000);

    }

    private void Dispatcher() {
        Boolean isRegister = false;
        Boolean isLogin = Hawk.contains("token");
                if (Hawk.contains("isRegister"))
                    isRegister = Hawk.get("isRegister");
        if (isRegister && isLogin)
            startActivity(new Intent(this,LocationOnMapActivity.class));
        else if (isRegister)
            startActivity(new Intent(this,LoginActivity.class));
        else
            startActivity(new Intent(this,RegisterActivity.class));
        finish();
    }

    private void checkDevice() {
        // Check if GPS is available
        if (!SmartLocation.with(mContext).location().state().isGpsAvailable())
        {
            //Go to GPS Activity And Enable that

            startActivityForResult
                    (new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS),150);

        }

//        // Check if Network is available
//        if (!SmartLocation.with(mContext).location().state().isNetworkAvailable())
//        {
//            //Go to WIFI Activity And Enable that
//
//            startActivityForResult
//                    (new Intent(Settings.ACTION_WIFI_SETTINGS),160);
//
//        }
    }

    private void startServices() {
        Intent intent = new Intent(mContext, LocationService.class);
        startService(intent);
    }

    private void checkPermission() {

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions
                    (this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1500);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        checkDevice();
    }
}
