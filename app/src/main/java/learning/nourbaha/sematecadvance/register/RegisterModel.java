package learning.nourbaha.sematecadvance.register;

import com.orhanobut.hawk.Hawk;

import learning.nourbaha.sematecadvance.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Nourbaha on 2/16/2018.
 */

public class RegisterModel implements RegisterContract.Model {
    private RegisterContract.Presenter presenter;

    @Override
    public void attachPresenter(RegisterContract.Presenter presenter) {

        this.presenter = presenter;
    }

    @Override
    public void register(String user, String pass, String name_family, String mobile) {
        Constants.webInterface.register("register",user,pass,name_family,mobile).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Hawk.put("isRegister",true);
                presenter.onRegisterSuccess();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                presenter.onRegisterFailed("Register is failed - Error message : "+t.getMessage());
            }
        });
    }
}
