package learning.nourbaha.sematecadvance.register;

import learning.nourbaha.sematecadvance.login.LoginContract;

/**
 * Created by Nourbaha on 2/16/2018.
 */

public interface RegisterContract {
    interface View {
        void onRegisterSuccess();
        void onRegisterFailed(String errorMessage);
        void onProgress(Boolean show);
    }

    interface Presenter {
        void attachView(View view);
        void detach();
        void register(String user, String pass, String confirmPass, String name_family, String mobile);
        void onRegisterSuccess();
        void onRegisterFailed(String errorMessage);
    }

    interface Model {
        void attachPresenter(Presenter presenter);
        void register(String user, String pass, String name_family, String mobile);
    }
}
