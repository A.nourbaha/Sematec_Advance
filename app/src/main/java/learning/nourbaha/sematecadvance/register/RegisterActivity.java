package learning.nourbaha.sematecadvance.register;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import learning.nourbaha.sematecadvance.BaseActivity;
import learning.nourbaha.sematecadvance.PublicMethods;
import learning.nourbaha.sematecadvance.R;
import learning.nourbaha.sematecadvance.login.LoginActivity;
import learning.nourbaha.sematecadvance.utils.views.MyButton;
import learning.nourbaha.sematecadvance.utils.views.MyEditText;

public class RegisterActivity extends BaseActivity implements RegisterContract.View, View.OnClickListener {
    RegisterPresenter presenter = new RegisterPresenter();

    MyEditText user;
    MyEditText pass;
    MyEditText confirmPass;
    MyEditText name_family;
    MyEditText mobile;
    MyButton register;

    ProgressDialog progressDialog ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        bindView();
    }

    private void bindView() {
        user = (MyEditText) findViewById(R.id.user);
        pass = (MyEditText) findViewById(R.id.pass);
        confirmPass = (MyEditText) findViewById(R.id.confirmPass);
        name_family = (MyEditText) findViewById(R.id.name_family);
        mobile = (MyEditText) findViewById(R.id.mobile);
        register = (MyButton) findViewById(R.id.register);
        progressDialog = new ProgressDialog(mContext);
        register.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.attachView(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.detach();
    }

    @Override
    public void onRegisterSuccess() {
        PublicMethods.showToast
                (mContext,"Register successfully");
        //Go to next page
        Intent intent = new Intent(mContext, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onRegisterFailed(String errorMessage) {
        PublicMethods.showToast(mContext,errorMessage);
    }

    @Override
    public void onProgress(Boolean show) {
        if(show)
            progressDialog.show();
        else
            progressDialog.dismiss();
    }

    @Override
    public void onClick(View view) {
        presenter.register
                (user.text(),pass.text(),confirmPass.text(),
                        name_family.text(),mobile.text());
    }
}
