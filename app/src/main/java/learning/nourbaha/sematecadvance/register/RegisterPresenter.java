package learning.nourbaha.sematecadvance.register;

/**
 * Created by Nourbaha on 2/16/2018.
 */

public class RegisterPresenter implements RegisterContract.Presenter {
    private RegisterContract.View view;
    private RegisterModel model = new RegisterModel();

    @Override
    public void attachView(RegisterContract.View view) {
        this.view = view;
        model.attachPresenter(this);
    }

    @Override
    public void detach() {
        this.view = null;
    }

    @Override
    public void register(String user, String pass, String confirmPass, String name_family, String mobile) {

        if (pass.compareTo(confirmPass) == 0)
            model.register(user,pass,name_family,mobile);
        else
            onRegisterFailed("تایید کلمه عبور صحیح وارد نشده است");
    }

    @Override
    public void onRegisterSuccess() {
        view.onRegisterSuccess();
    }

    @Override
    public void onRegisterFailed(String errorMessage) {
        view.onRegisterFailed(errorMessage);
    }
}
