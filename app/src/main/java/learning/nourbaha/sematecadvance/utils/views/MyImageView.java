package learning.nourbaha.sematecadvance.utils.views;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import com.squareup.picasso.Picasso;

/**
 * Created by Nourbaha on 2/9/2018.
 */

public class MyImageView extends AppCompatImageView {
    private Context mContext;

    public MyImageView(Context context) {
        super(context);
        mContext = context;
    }

    public MyImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public void Load(String url)
    {
        Picasso.with(mContext).load(url).into(this);
    }
}
