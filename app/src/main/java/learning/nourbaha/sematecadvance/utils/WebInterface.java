package learning.nourbaha.sematecadvance.utils;

import learning.nourbaha.sematecadvance.webmodels.LeagueTable;
import learning.nourbaha.sematecadvance.webmodels.TokenPojoModel;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Nourbaha on 2/16/2018.
 */

public interface WebInterface {

    @FormUrlEncoded
    @POST("advance.php")
    Call<Void> register(
            @Field("job") String job,
            @Field("username") String username,
            @Field("password") String password,
            @Field("name_family") String name_family,
            @Field("mobile") String mobile
    );

    @FormUrlEncoded
    @POST("advance.php")
    Call<TokenPojoModel> login(
            @Field("job") String job,
            @Field("username") String username,
            @Field("password") String password
    );

    @GET("leagueTable/")
    Call<LeagueTable> loadLeagueTable(
            @Query("matchday") int day
    );

}
