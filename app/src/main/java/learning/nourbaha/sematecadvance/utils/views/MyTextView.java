package learning.nourbaha.sematecadvance.utils.views;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import learning.nourbaha.sematecadvance.MyApplication;

/**
 * Created by Nourbaha on 2/9/2018.
 */

public class MyTextView extends AppCompatTextView {
    public MyTextView(Context context) {
        super(context);
        if (!isInEditMode())
            this.setTypeface(MyApplication.typeface);
    }

    public MyTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode())
            this.setTypeface(MyApplication.typeface);
    }
}
