package learning.nourbaha.sematecadvance;

import android.content.Context;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;

/**
 * Created by Nourbaha on 2/3/2018.
 */

public class PublicMethods {

    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static Boolean isOnline() {
        try {
            Process p1 = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.com");
            int returnVal = p1.waitFor();
            return (returnVal == 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static String getToken()
    {
        return Hawk.get("token","");
    }

    public static Integer convertTempFtoC(Integer fTemp) {
        Double c;
        c = (fTemp - 32) / 1.8;

//        DecimalFormat df = new DecimalFormat("#");
//        Double.valueOf(df.format(c));
        return Integer.valueOf(c.intValue());
    }
}
