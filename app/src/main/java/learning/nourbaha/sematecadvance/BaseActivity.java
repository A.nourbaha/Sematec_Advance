package learning.nourbaha.sematecadvance;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Nourbaha on 2/3/2018.
 */

public class BaseActivity extends AppCompatActivity{
    public Context mContext = this;
    public Activity mActivity = this;
    ProgressDialog progressDialog ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        progressDialog = new ProgressDialog(mContext);
    }

    public void onProgress(Boolean show) {
        if (show)
            progressDialog.show();
        else
            progressDialog.cancel();
    }
}
