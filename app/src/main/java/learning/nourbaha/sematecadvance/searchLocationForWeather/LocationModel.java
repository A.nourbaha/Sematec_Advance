package learning.nourbaha.sematecadvance.searchLocationForWeather;

import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import cz.msebera.android.httpclient.Header;
import learning.nourbaha.sematecadvance.R;
import learning.nourbaha.sematecadvance.webmodels.YahooWeatherModel.YahooWeatherModel;

/**
 * Created by Nourbaha on 3/5/2018.
 */

public class LocationModel implements LocationContract.Model {
    private LocationContract.Presenter presenter;

    @Override
    public void attachPresenter(LocationContract.Presenter presenter) {

        this.presenter = presenter;
    }

    @Override
    public void getWeather(double lat, double lon) {
        String url =
                "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(SELECT%20woeid%20FROM%20geo.places%20WHERE%20text%3D%22(" +
                        Double.toString(lat) + "%2C" + Double.toString(lon) + ")%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";

//        "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(SELECT%20woeid%20FROM%20geo.places%20WHERE%20text%3D%22(41.2%2C41.2)%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";

        getYahooWeather(url);

    }

    private void getYahooWeather(String url) {

        if (url.length() == 0)
            return;

        Log.d("weather_debuging", url);

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new TextHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.d("weather_debuging", "Failure");
                presenter.onGetWeatherFailed(throwable.getMessage());
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Log.d("weather_debuging", "Success");
                Log.d("weather_debuging", responseString);
                Gson gson = new Gson();
                YahooWeatherModel yahooWeather =
                        gson.fromJson(responseString, YahooWeatherModel.class);
                presenter.onGetWeatherSuccess(yahooWeather);
            }
        });
    }
}
