package learning.nourbaha.sematecadvance.searchLocationForWeather;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.orhanobut.hawk.Hawk;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.location.config.LocationParams;
import learning.nourbaha.sematecadvance.PublicMethods;
import learning.nourbaha.sematecadvance.R;
import learning.nourbaha.sematecadvance.splashscreen.SplashScreenActivity;
import learning.nourbaha.sematecadvance.utils.views.MyButton;
import learning.nourbaha.sematecadvance.utils.views.MyImageView;
import learning.nourbaha.sematecadvance.webmodels.YahooWeatherModel.YahooWeatherModel;

public class LocationOnMapActivity extends FragmentActivity implements
        OnMapReadyCallback, View.OnClickListener,
        GoogleMap.OnCameraIdleListener, LocationContract.View {

    private Context mContext = this;
    private MyButton logout;
    private MyImageView mapMarker;
    private Boolean close = false;
    private GoogleMap mMap;
    private ProgressDialog progressDialog;
    private LocationPresenter presenter = new LocationPresenter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        logout = (MyButton) findViewById(R.id.logout);
        mapMarker = (MyImageView) findViewById(R.id.mapMarker);

        progressDialog = new ProgressDialog(mContext);

        presenter.attachView(this);
        logout.setOnClickListener(this);
        mapMarker.setOnClickListener(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnCameraIdleListener(this);

        try {
            boolean success =
                    mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle
                            (mContext, R.raw.mapstyle));
        } catch (Exception e) {
        }

//        mMap.getUiSettings().setAllGesturesEnabled(true);
//        mMap.getUiSettings().setCompassEnabled(true);
//        mMap.getUiSettings().setMapToolbarEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
//        mMap.getUiSettings().setTiltGesturesEnabled(true);
//        mMap.getUiSettings().setZoomGesturesEnabled(true);
        mMap.getUiSettings().setScrollGesturesEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setRotateGesturesEnabled(true);
        mMap.getUiSettings().setIndoorLevelPickerEnabled(true);


        SmartLocation.with(this).location().oneFix().config(LocationParams.BEST_EFFORT).
                start(new OnLocationUpdatedListener() {
                    @Override
                    public void onLocationUpdated(Location location) {

                        // Add a marker in Sydney and move the camera
                        LatLng myLocation = new LatLng(location.getLatitude(), location.getLongitude());
                        mMap.addMarker(new MarkerOptions().position(myLocation).title("My location"));
//                        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 16));
                    }
                });
    }

    @Override
    public void onCameraIdle() {
        showCurrentLocation();
//        PublicMethods.showToast(mContext, "Zoom level :" + mMap.getCameraPosition().zoom);
    }

    void showCurrentLocation() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                double lat = mMap.getCameraPosition().target.latitude;
                double lon = mMap.getCameraPosition().target.longitude;
                PublicMethods.showToast(mContext, "Lat : " + lat + "\nLon : " + lon
                        + "\nZoom level : " + mMap.getCameraPosition().zoom);
            }
        }, 1000);

    }

    @Override
    public void onBackPressed() {
        if (close)
            super.onBackPressed();
        else {
            close = true;
            PublicMethods.showToast(mContext, "برای خروج، دوباره کلید بازگشت را فشار دهید");
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                close = false;
            }
        }, 2000);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.logout) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Hawk.delete("token");
                    startActivity(new Intent(mContext, SplashScreenActivity.class));
                    finish();
                }
            }, 1000);
        } else if (v.getId() == R.id.mapMarker) {
            double lat = mMap.getCameraPosition().target.latitude;
            double lon = mMap.getCameraPosition().target.longitude;
            presenter.getWeather(lat, lon);
        }
    }

    @Override
    public void onProgress(Boolean show) {
        if (show)
            progressDialog.show();
        else
            progressDialog.cancel();
    }

    @Override
    public void onGetWeatherSuccess(YahooWeatherModel yahooWeather) {
        String temp = (PublicMethods.convertTempFtoC
                (Integer.parseInt(yahooWeather.getQuery().getResults()
                        .getChannel().getItem().getCondition().getTemp())) + "C" + "°");
        String type = yahooWeather.getQuery().getResults()
                        .getChannel().getItem().getCondition().getText();
        String maxTemp = (PublicMethods.convertTempFtoC
                (Integer.parseInt(yahooWeather.getQuery()
                        .getResults().getChannel().getItem().getForecast().get(0).getHigh())) + "C" + "°");
        String minTemp = (PublicMethods.convertTempFtoC
                (Integer.parseInt(yahooWeather.getQuery()
                        .getResults().getChannel().getItem().getForecast().get(0).getLow())) + "C" + "°");
        String city = (yahooWeather.getQuery()
                .getResults().getChannel().getLocation().getCity());
        String country = (yahooWeather.getQuery()
                .getResults().getChannel().getLocation().getCountry());

        PublicMethods.showToast(mContext, "Country : " + country +
                "\nCity : " + city + "\nTemp : " + temp
                + "\nType : " + type+ "\nMax Temp : " + maxTemp + "\nMin Temp : " + minTemp);
    }

    @Override
    public void onGetWeatherFailed(String message) {
        PublicMethods.showToast(mContext, "Get Weather Failed - message :\n" + message);
    }

}
