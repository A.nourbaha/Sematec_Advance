package learning.nourbaha.sematecadvance.searchLocationForWeather;

import learning.nourbaha.sematecadvance.register.RegisterContract;
import learning.nourbaha.sematecadvance.webmodels.YahooWeatherModel.YahooWeatherModel;

/**
 * Created by Nourbaha on 3/5/2018.
 */

public interface LocationContract {
    interface View {
        void onProgress(Boolean show);
        void onGetWeatherSuccess(YahooWeatherModel yahooWeather);
        void onGetWeatherFailed(String message);
    }

    interface Presenter {
        void attachView(View view);
        void detach();
        void getWeather(double lat,double lon);
        void onGetWeatherSuccess(YahooWeatherModel yahooWeather);
        void onGetWeatherFailed(String message);
    }

    interface Model {
        void attachPresenter(Presenter presenter);
        void getWeather(double lat,double lon);

    }
}
