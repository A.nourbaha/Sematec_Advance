package learning.nourbaha.sematecadvance.searchLocationForWeather;

import learning.nourbaha.sematecadvance.webmodels.YahooWeatherModel.YahooWeatherModel;

/**
 * Created by Nourbaha on 3/5/2018.
 */

public class LocationPresenter implements LocationContract.Presenter {
    private LocationContract.View view;
    private LocationModel model = new LocationModel();

    @Override
    public void attachView(LocationContract.View view) {

        this.view = view;
        model.attachPresenter(this);
    }

    @Override
    public void detach() {
        this.view = null;
    }

    @Override
    public void getWeather(double lat, double lon) {
        view.onProgress(true);
        model.getWeather(lat,lon);
    }

    @Override
    public void onGetWeatherSuccess(YahooWeatherModel yahooWeather) {
        view.onGetWeatherSuccess(yahooWeather);
        view.onProgress(false);
    }

    @Override
    public void onGetWeatherFailed(String message) {
        view.onGetWeatherFailed(message);
        view.onProgress(false);
    }
}
