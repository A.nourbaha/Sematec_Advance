package learning.nourbaha.sematecadvance.userreport;

import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.view.View;

import com.orhanobut.hawk.Hawk;

import learning.nourbaha.sematecadvance.BaseActivity;
import learning.nourbaha.sematecadvance.PublicMethods;
import learning.nourbaha.sematecadvance.R;
import learning.nourbaha.sematecadvance.splashscreen.SplashScreenActivity;
import learning.nourbaha.sematecadvance.utils.views.MyButton;

public class UserReportActivity extends BaseActivity implements View.OnClickListener {
    MyButton logout;
    Boolean close = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_report);

        bindViews();
    }

    private void bindViews() {
        logout = (MyButton) findViewById(R.id.logout);
        logout.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.logout) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Hawk.delete("token");
                    startActivity(new Intent(mContext, SplashScreenActivity.class));
                    finish();
                }
            }, 1000);
        }
    }

    @Override
    public void onBackPressed() {
        if (close)
            super.onBackPressed();
        else {
            close = true;
            PublicMethods.showToast(mContext,"برای خروج، دوباره کلید بازگشت را فشار دهید");
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                close = false;
            }
        },2000);

    }
}
